FieldSonar-server
====

v2.2がリリースされました. コメント取得のエンドポイント[FieldSonar-api-server](https://fieldsonar.herokuapp.com/api/v1/comments)です.
Herokuに上げていますが, AWSにも上げられます. SSLを有効にするための起動コマンドはbenri commandの節を見てください

## Demo

![スクリーンショット](./public/screen_shot.jpeg)

## 機能

#### コメントの投稿

`api/v1/comment` へのPOST

#### 画像の保存

機械学習画像を保存できます.

#### LINEからの投稿を受け付ける

チャットボットのサーバーにもなります

## benri command

AWSへアクセス
```
ssh -i "koizumiJikkenn.pem.txt" ec2-user@ec2-54-64-102-195.ap-northeast-1.compute.amazonaws.com
```

nodeを有効にする
```
source ~/.nvm/nvm.sh
```

Rails serverをSSL付きで起動
```
SSL=true bundle exec rails s -b 0.0.0.0
```

Herokuからデータをcsvで落としてくる.
[herokuのpsqlに直でアクセスし、CSVにデータを書き出す](http://nafuruby.hatenablog.com/entry/2014/12/03/175256)
```
heroku config

psql DATABASE_URL

\copy (SELECT first_name, last_name, email FROM users) TO dump.csv CSV DELIMITER ','
```

## 利用方法

Heroku上の`https://fieldsonar.herokuapp.com/api/v1/comments`をエンドポイントにするだけで利用できます

## v2.1からの変更点
* LINEでの応答をボタンで返せるようになりました.
* スタンプが帰ってきます
* LINEからはユーザの名前を登録できます

## 課題
* ブラウザ版がスマホ対応できていない. 画面小さい.
