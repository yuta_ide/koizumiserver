Rails.application.routes.draw do
  resources :images
  resources :comments
  resources :users
  post '/callback' => 'callback#callback'
  namespace :api, { format: 'json' }  do
    namespace :v1 do
      resources :comments
      resources :images
      get 'show_picture' => 'images#show_picture', :as => :show_picture
    end
  end
end
