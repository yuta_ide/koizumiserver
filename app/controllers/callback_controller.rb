class CallbackController < ApplicationController
  skip_before_filter :verify_authenticity_token
  before_action :get_info_from_message, only: [:callback]

  CHANNEL_SECRET = ENV['CHANNEL_SECRET']
  OUTBOUND_PROXY = ENV['OUTBOUND_PROXY']
  CHANNEL_ACCESS_TOKEN = ENV['CHANNEL_ACCESS_TOKEN']

  def callback
    logger.debug '[CALLBACK]<callback> fire'
    render nothing: true, status: 470 unless is_validate_signature

    reply_content = []
    @comment = Comment.where(user_id: @user.id, is_saved: false)
    if @comment == []
      logger.debug '[CALLBACK]<callback> @comment == [] fire'
      @comment = Comment.create(user_id: @user.id, is_saved: false)
    else
      logger.debug '[CALLBACK]<callback> @comment != [] fire'
      @comment = @comment.first
    end
    case @event_type
    when 'message'
      if @event['message']['text'] == 'リセット'
        logger.debug 'リセット'
        @user.update(state: '')
      end
      case @state
      when 'before_register'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        reply = reply_composer('君の名は？')
        @user.update(state: 'registering')
      when 'registering'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        your_name = @event['message']['text']
        @user.update(name: your_name)
        @user.update(state: 'stand_by')
      when 'stand_by'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        reply = reply_composer('対象物は？')
        @user.update(state: 'asked_object')
      when 'asked_object'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        object = @event['message']['text']
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        logger.debug "[CALLBACK]<callback> @comment.user_id: #{@comment.user_id}"
        logger.debug "[CALLBACK]<callback> object: #{object}"
        @comment.update(name: object)
        reply = reply_composer('感想は？')
        @user.update(state: 'asked_content')
      when 'asked_content'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        content = @event['message']['text']
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        @comment.update(content: content)
        reply = reply_composer('位置は？ 位置情報送信機能で送ってください')
        @user.update(state: 'asked_location')
      when 'asked_location'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        latitude = @event['message']['latitude']
        longitude = @event['message']['longitude']
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        @comment.update(latitude: latitude, longitude: longitude)
        reply = feeling_choice_composer
        @user.update(state: 'asked_impression')
      when 'asked_impression'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        impression = @event['message']['text']
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        @comment.update(feeling: impression)
        reply = reply_composer('マクロビューアーは？ 使った=>0, 使ってない=>1')
        @user.update(state: 'asked_macro')
      when 'asked_macro'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        is_macro = @event['message']['text']
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        @comment.update(is_micro: is_macro)
        reply = reply_composer('地図モードと双対モードのどちらが参考になった？ 使ってない=>0, 地図=>1, 双対=>2')
        @user.update(state: 'asked_page')
      when 'asked_page'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        page = @event['message']['text']
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        logger.debug "[CALLBACK]<callback> page: #{page}"
        @comment.update(page: page, is_saved: true)
        reply = reply_composer('おしまい. 待機状態に戻ります. 何かまた見つけたら呼びかけてね')
        reply_content.concat(reply)
        logger.debug "[CALLBACK]<callback> reply: #{reply}"
        reply = sticker_composer
        logger.debug "[CALLBACK]<callback> reply: #{reply}"
        @user.update(state: 'stand_by')
      end
    when 'postback'
      logger.debug "[CALLBACK]<callback> @event_type: #{@event_type}"
      case @state
      when 'asked_impression'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        impression = @event['postback']['data']
        logger.debug "[CALLBACK]<callback> impression: #{impression}"
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        @comment.update(feeling: impression)
        reply = macroviewer_choice_composer
        @user.update(state: 'asked_macro')
      when 'asked_macro'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        is_macro = @event['postback']['data']
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        @comment.update(is_micro: is_macro)
        reply = reference_choice_composer
        @user.update(state: 'asked_page')
      when 'asked_page'
        logger.debug "[CALLBACK]<callback> @state: #{@state}"
        page = @event['postback']['data']
        logger.debug "[CALLBACK]<callback> @comment: #{@comment}"
        logger.debug "[CALLBACK]<callback> page: #{page}"
        @comment.update(page: page, is_saved: true)
        reply = reply_composer('おしまい. 待機状態に戻ります. 何かまた見つけたら呼びかけてね')
        reply_content.concat(reply)
        logger.debug "[CALLBACK]<callback> reply: #{reply}"
        reply = sticker_composer
        logger.debug "[CALLBACK]<callback> reply: #{reply}"
        @user.update(state: 'stand_by')
      end
    end
    logger.debug "[CALLBACK]<callback> reply: #{reply}"
    reply_content.concat(reply)
    logger.debug "[CALLBACK]<callback> reply_content: #{reply_content}"
    client = LineClient.new(CHANNEL_ACCESS_TOKEN, OUTBOUND_PROXY)
    res = client.reply(@replyToken, reply_content)

    if res.status == 200
      logger.info(success: res)
    else
      logger.info(fail: res)
    end

    logger.debug "-------------------------Request Done-----------------------------------\n"
    render nothing: true, status: :ok
  end

  private

  def is_validate_signature
    signature = request.headers['X-LINE-Signature']
    http_request_body = request.raw_post
    hash = OpenSSL::HMAC.digest(OpenSSL::Digest::SHA256.new, CHANNEL_SECRET, http_request_body)
    signature_answer = Base64.strict_encode64(hash)
    signature == signature_answer
  end

  def get_info_from_message
    logger.debug ' [CALLBACK] <get_info_from_message> fire'
    @event = params['events'][0]
    @event_type = @event['type']
    @replyToken = @event['replyToken']
    @source = @event['source']['type']
    @line_id = @event['source']['userId']
    @user = User.find_or_initialize_by(line_id: @line_id)
    if @user.new_record?
      logger.debug ' [CALLBACK] <get_info_from_message> user初期化'
      @user.state = 'before_register'
      @user.save!
    end
    @state = @user.state
  end

  def reply_composer(text)
    reply_content = [
      {
        'type' => 'text',
        'text' => text
      }
    ]
    reply_content
  end

  def sticker_composer
    reply_content = [
      {
        'type' => 'sticker',
        'packageId' => '1',
        'stickerId' => '116'
      }
    ]
    reply_content
  end

  def feeling_choice_composer
    reply_content = [
      {
        'type' => 'template',
        'altText' => '印象は？良い=>0, 悪い=>1, 疑問=>2, 不明=>3',
        'template' => {
          'type' => 'buttons',
          'title' => '印象は？',
          'text' => '選んでね',
          'actions' => [
            {
              'type' => 'postback',
              'label' => '良い',
              'data' => '0'
            },
            {
              'type' => 'postback',
              'label' => '悪い',
              'data' => '1'
            },
            {
              'type' => 'postback',
              'label' => '疑問',
              'data' => '2'
            },
            {
              'type' => 'postback',
              'label' => '不明',
              'data' => '3'
            }
          ]
        }
      }
    ]
    reply_content
  end

  def macroviewer_choice_composer
    logger.debug '[CALLBACK]<macroviewer_choice_compose> fire'
    reply_content = [
      {
        'type' => 'template',
        'altText' => 'マクロビューアーは？',
        'template' => {
          'type' => 'buttons',
          'title' => 'マクロビューアーを参考にした？',
          'text' => '選んでね',
          'actions' => [
            {
              'type' => 'postback',
              'label' => '使った',
              'data' => '0'
            },
            {
              'type' => 'postback',
              'label' => '使っていない',
              'data' => '1'
            }
          ]
        }
      }
    ]
    reply_content
  end

  def reference_choice_composer
    logger.debug '[CALLBACK]<macroviewer_choice_compose> fire'
    reply_content = [
      {
        'type' => 'template',
        'altText' => '地図モードと次元圧縮モードのどちらが参考になった？ 使ってない=>0, 地図=>1, 双対=>2',
        'template' => {
          'type' => 'buttons',
          'title' => 'どの機能を参考にした?',
          'text' => '選んでね',
          'actions' => [
            {
              'type' => 'postback',
              'label' => '使っていない',
              'data' => '0'
            },
            {
              'type' => 'postback',
              'label' => '地図',
              'data' => '1'
            },
            {
              'type' => 'postback',
              'label' => 'キーワード散布図',
              'data' => '2'
            }
          ]
        }
      }
    ]
    reply_content
  end
end
