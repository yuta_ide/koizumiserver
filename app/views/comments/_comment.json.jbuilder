json.extract! comment, :id, :name, :content, :longitude, :latitude, :feeling, :page, :is_micro, :user_id, :created_at, :updated_at
json.url comment_url(comment, format: :json)