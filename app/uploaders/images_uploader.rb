# encoding: utf-8
class ImagesUploader < CarrierWave::Uploader::Base
  storage :file
  def store_dir
    "uploads"
  end
end
