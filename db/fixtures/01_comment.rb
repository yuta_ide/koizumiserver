Comment.seed do |s|
  s.name = "駅"
  s.content = "ラーメン屋さんとうどん屋さんがある"
  s.longitude = 124.2
  s.latitude = 111.2
  s.feeling = 0
  s.page = 1
  s.is_micro = false
  s.user_id = 1
end

Comment.seed do |s|
  s.name = "郵便局"
  s.content = "楽しそうな人たちがたくさんいる。今日はパーティなのかもしれない"
  s.longitude = 14.2
  s.latitude = 1.2
  s.feeling = 1
  s.page = 2
  s.is_micro = true
  s.user_id = 2
end

Comment.seed do |s|
  s.name = "駅"
  s.content = "キーボード演奏を楽しんでいるバンドマンがいる。すごく上手くて感動した"
  s.longitude = 152.2
  s.latitude = 113.2
  s.feeling = 0
  s.page = 1
  s.is_micro = false
  s.user_id = 1
end
