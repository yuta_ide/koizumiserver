class AddStatesToUsers < ActiveRecord::Migration
  def change
    add_column :users, :line_id, :string
    add_column :users, :state, :string
  end
end
