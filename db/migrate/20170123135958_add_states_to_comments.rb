class AddStatesToComments < ActiveRecord::Migration
  def change
    add_column :comments, :is_saved, :boolean
  end
end
