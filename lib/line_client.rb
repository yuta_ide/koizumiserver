class LineClient
  END_POINT = "https://api.line.me"

  def initialize(channel_access_token, proxy = nil)
    puts "[LINE CLIENT] <initialize> fire"
    @channel_access_token = channel_access_token
    @proxy = proxy
  end

  def post(path, data)
    puts "[LINE CLIENT] <post> fire"
    Rails.logger.debug "[LINE CLIENT] <post> data: #{data}"
    client = Faraday.new(:url => END_POINT) do |conn|
      conn.request :json
      conn.response :json, :content_type => /\bjson$/
      conn.adapter Faraday.default_adapter
      conn.proxy @proxy
    end

    res = client.post do |request|
      request.url path
      request.headers = {
        'Content-type' => 'application/json',
        'Authorization' => "Bearer #{@channel_access_token}"
      }
      request.body = data
    end
    res
  end

  def reply(replyToken, reply_content)
    puts "[LINE CLIENT] <reply> fire"
    body = {
      "replyToken" => replyToken ,
      "messages" => reply_content
    }
    post('/v2/bot/message/reply', body.to_json)
  end

end
